#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include "speed_print.h"

void print_results(const char *s, uint64_t *t, size_t tlen, FILE* fPtr) {
  int i;

  if(tlen < 2) {
    fprintf(stderr, "ERROR: Need a least two cycle counts!\n");
    return;
  }
  long int acc=0;

  fprintf(fPtr,"%s", s);
  for(i=0;i<tlen;i++){
    acc += t[i];
    fprintf(fPtr, "%lu ", t[i]);
  }
  fprintf(fPtr, "\n%s%lu\n", s, acc/tlen);
}
