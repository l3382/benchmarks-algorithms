#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

int main()
{
    FILE * fPtr,*fPtr2,*f512,*f51290,*f768,*f76890,*f1024,*f102490;
    char ch;
    system("rm ../KYBERTXT/output.txt");
    system("rm ../KYBERTXT/kyber512.txt");
    system("rm ../KYBERTXT/kyber51290.txt");
    system("rm ../KYBERTXT/kyber768.txt");
    system("rm ../KYBERTXT/kyber76890.txt");
    system("rm ../KYBERTXT/kyber1024.txt");
    system("rm ../KYBERTXT/kyber102490.txt");
    system("make speed");
    system("./test_speed512");
    system("./test_speed512-90s");
    system("./test_speed768");
    system("./test_speed768-90s");
    system("./test_speed1024");
    system("./test_speed1024-90s");
    fPtr2 = fopen("../KYBERTXT/output.txt", "r");
    f512 = fopen("../KYBERTXT/kyber512.txt", "a");
    /* fopen() return NULL if last operation was unsuccessful */
    if(f512 == NULL)
    {
      /* File not created hence exit */
      printf("Unable to create file.\n");
      exit(EXIT_FAILURE);
    }
    do{
        ch = getc(fPtr2);
        if(ch!='='){
          fprintf(f512,"%c", ch);
 	}
        // Checking if character is not EOF.
        // If it is EOF stop eading.
    } while (ch != '=');
    fprintf(f512,"SECURITY:128", ch);
    fclose(f512);
    
    f51290 = fopen("../KYBERTXT/kyber51290.txt", "a");
    /* fopen() return NULL if last operation was unsuccessful */
    if(f51290 == NULL)
    {
      /* File not created hence exit */
      printf("Unable to create file.\n");
      exit(EXIT_FAILURE);
    }
    do{
        ch = getc(fPtr2);
        if(ch!='='){
          fprintf(f51290,"%c", ch);
 	}
        // Checking if character is not EOF.
        // If it is EOF stop eading.
    } while (ch != '=');
    fprintf(f51290,"SECURITY:128", ch);
    fclose(f51290);
    
    
    f768 = fopen("../KYBERTXT/kyber768.txt", "a");
    /* fopen() return NULL if last operation was unsuccessful */
    if(f768 == NULL)
    {
      /* File not created hence exit */
      printf("Unable to create file.\n");
      exit(EXIT_FAILURE);
    }
    do{
        ch = getc(fPtr2);
        if(ch!='='){
          fprintf(f768,"%c", ch);
 	}
        // Checking if character is not EOF.
        // If it is EOF stop eading.
    } while (ch != '=');
    fprintf(f768,"SECURITY:192", ch);
    fclose(f768);
    
    
    f76890 = fopen("../KYBERTXT/kyber76890.txt", "a");
    /* fopen() return NULL if last operation was unsuccessful */
    if(f76890 == NULL)
    {
      /* File not created hence exit */
      printf("Unable to create file.\n");
      exit(EXIT_FAILURE);
    }
    do{
        ch = getc(fPtr2);
        if(ch!='='){
          fprintf(f76890,"%c", ch);
 	}
        // Checking if character is not EOF.
        // If it is EOF stop eading.
    } while (ch != '=');
    fprintf(f76890,"SECURITY:192", ch);
    fclose(f76890);
    
    f1024 = fopen("../KYBERTXT/kyber1024.txt", "a");
    /* fopen() return NULL if last operation was unsuccessful */
    if(f1024 == NULL)
    {
      /* File not created hence exit */
      printf("Unable to create file.\n");
      exit(EXIT_FAILURE);
    }
    do{
        ch = getc(fPtr2);
        if(ch!='='){
          fprintf(f1024,"%c", ch);
 	}
        // Checking if character is not EOF.
        // If it is EOF stop eading.
    } while (ch != '=');
    fprintf(f1024,"SECURITY:256", ch);
    fclose(f1024);
    
    f102490 = fopen("../KYBERTXT/kyber102490.txt", "a");
    /* fopen() return NULL if last operation was unsuccessful */
    if(f102490 == NULL)
    {
      /* File not created hence exit */
      printf("Unable to create file.\n");
      exit(EXIT_FAILURE);
    }
    do{
        ch = getc(fPtr2);
        if(ch!='='){
          fprintf(f102490,"%c", ch);
 	}
        // Checking if character is not EOF.
        // If it is EOF stop eading.
    } while (ch != '=');
    fprintf(f102490,"SECURITY:256", ch);
    fclose(f102490);
    
    fclose(fPtr2);
    return 0;
}
