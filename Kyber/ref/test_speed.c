#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "kem.h"
#include "kex.h"
#include "params.h"
#include "indcpa.h"
#include "polyvec.h"
#include "poly.h"
#include "speed_print.h"

#define NTESTS 1000

uint64_t t[NTESTS];
uint8_t seed[KYBER_SYMBYTES] = {0};

int main(int argc , char ** argv)
{
  unsigned int i;
  uint8_t pk[CRYPTO_PUBLICKEYBYTES];
  uint8_t sk[CRYPTO_SECRETKEYBYTES];
  uint8_t ct[CRYPTO_CIPHERTEXTBYTES];
  uint8_t key[CRYPTO_BYTES];
  uint8_t kexsenda[KEX_AKE_SENDABYTES];
  uint8_t kexsendb[KEX_AKE_SENDBBYTES];
  uint8_t kexkey[KEX_SSBYTES];
  polyvec matrix[KYBER_K];
  poly ap;

  /* File pointer to hold reference to our file */
  FILE * fPtr;
  clock_t t1;

  /* 
    * Open file in w (write) mode. 
    * "data/file1.txt" is complete path to create file
    */



  fPtr = fopen("../KYBERTXT/output.txt", "a");

  /* fopen() return NULL if last operation was unsuccessful */
  if(fPtr == NULL)
  {
      /* File not created hence exit */
      printf("Unable to create file.\n");
      exit(EXIT_FAILURE);
  }

  fprintf(fPtr, "PUBLICKEYBYTES:%d \n", CRYPTO_PUBLICKEYBYTES);
  fprintf(fPtr, "SECRETKEYBYTES:%d \n", CRYPTO_SECRETKEYBYTES);
  fprintf(fPtr, "CIPHERTEXTBYTES:%d \n", CRYPTO_SECRETKEYBYTES);

  for(i=0;i<NTESTS;i++) {
    t1 = clock();
    indcpa_keypair(pk, sk);
    t[i] = clock() - t1;
  }
  print_results("GENERATIONTIME:", t, NTESTS, fPtr);

  for(i=0;i<NTESTS;i++) {
    t1 = clock();
    indcpa_enc(ct, key, pk, seed);
    t[i] = clock() - t1;
  }
  print_results("CHIFFREMENT:", t, NTESTS, fPtr);

  for(i=0;i<NTESTS;i++) {
    t1 = clock();
    indcpa_dec(key, ct, sk);
    t[i] = clock() - t1;
  }
  print_results("DECHIFFREMENT:", t, NTESTS, fPtr);
  fprintf(fPtr, "=");

    /* Close file to save file data */
    fclose(fPtr);


    /* Success message */
    printf("File created and saved successfully. :) \n");
  return 0;
}
