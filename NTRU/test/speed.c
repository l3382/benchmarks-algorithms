#include "../kem.h"
#include "../params.h"
#include "../randombytes.h"
#include "../poly.h"
#include "../sample.h"
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#define NTESTS 100


int main()
{
  unsigned char key_a[32], key_b[32];
  //poly r, a, b;
  unsigned char* pks = (unsigned char*) malloc(NTESTS*NTRU_PUBLICKEYBYTES);
  unsigned char* sks = (unsigned char*) malloc(NTESTS*NTRU_SECRETKEYBYTES);
  unsigned char* cts = (unsigned char*) malloc(NTESTS*NTRU_CIPHERTEXTBYTES);
  //unsigned char fgbytes[NTRU_SAMPLE_FG_BYTES];
  //unsigned char rmbytes[NTRU_SAMPLE_RM_BYTES];
  //uint16_t a1 = 0;
  int i;

  /* File pointer to hold reference to our file */
  FILE * fPtr;

  /* 
    * Open file in w (write) mode. 
    * "data/file1.txt" is complete path to create file
    */
  fPtr = fopen("output.txt", "w");

  /* fopen() return NULL if last operation was unsuccessful */
  if(fPtr == NULL)
  {
      /* File not created hence exit */
      printf("Unable to create file.\n");
      exit(EXIT_FAILURE);
  }

  fprintf(fPtr, "PUBLICKEYBYTES: %d \n", NTRU_PUBLICKEYBYTES);
  fprintf(fPtr, "SECRETKEYBYTES: %d \n", NTRU_SECRETKEYBYTES);
  fprintf(fPtr, "CIPHERTEXTBYTES: %d \n", NTRU_CIPHERTEXTBYTES);

  /*printf("-- api --\n\n");

  for(i=0; i<NTESTS; i++)
  {
    t[i] = cpucycles();
    crypto_kem_keypair(pks+i*NTRU_PUBLICKEYBYTES, sks+i*NTRU_SECRETKEYBYTES);
  }
  print_results("ntru_keypair: ", t, NTESTS);*/
  uint64_t t[NTESTS];
  int t1, t2, t3, average_key = 0, average_dec = 0, average_enc = 0;

  fprintf(fPtr, "GENERATION:");

  for(i=0; i<NTESTS; i++)
  {
    t3 = clock();
    crypto_kem_keypair(pks+i*NTRU_PUBLICKEYBYTES, sks+i*NTRU_SECRETKEYBYTES);
    t[i] = clock() - t3;
    fprintf(fPtr, "%ld ", t[i]);
		average_key = average_key + t[i];
  }

  fprintf(fPtr, "\n");

  fprintf(fPtr, "GENERATION: %d", average_key/NTESTS);

  fprintf(fPtr, "\n");

  fprintf(fPtr, "CHIFFREMENT:");

  for(i=0; i<NTESTS; i++)
  {
    t1 = clock();
    crypto_kem_enc(cts+i*NTRU_CIPHERTEXTBYTES, key_b, pks+i*NTRU_PUBLICKEYBYTES);
    t[i] = clock() - t1;
    fprintf(fPtr, "%ld ", t[i]);
		average_enc = average_enc + t[i];
  }

  fprintf(fPtr, "\n");

  fprintf(fPtr, "CHIFFREMENT: %d", average_enc/NTESTS);

  fprintf(fPtr, "\n");

  fprintf(fPtr, "DECHIFFREMENT:");

  for(i=0; i<NTESTS; i++)
  {
    t2 = clock();
    crypto_kem_dec(key_a, cts+i*NTRU_CIPHERTEXTBYTES, sks+i*NTRU_SECRETKEYBYTES);
    t[i] = clock() - t2;
    fprintf(fPtr, "%ld ", t[i]);
		average_dec = average_dec + t[i];
  }

  fprintf(fPtr, "\n");

  fprintf(fPtr, "DECHIFFREMENT: %d", average_dec/NTESTS);

  /*printf("-- internals --\n\n");

  randombytes(fgbytes, sizeof(fgbytes));
  sample_fg(&a, &b, fgbytes);
  poly_Z3_to_Zq(&a);
  poly_Z3_to_Zq(&b);

  for(i=0; i<NTESTS; i++)
  {
    t[i] = cpucycles();
    poly_Rq_mul(&r, &a, &b);
  }
  print_results("poly_Rq_mul: ", t, NTESTS);

  for(i=0; i<NTESTS; i++)
  {
    t[i] = cpucycles();
    poly_S3_mul(&r, &a, &b);
  }
  print_results("poly_S3_mul: ", t, NTESTS);

  // a as generated in test_polymul
  for(i=0; i<NTRU_N; i++)
    a1 += a.coeffs[i];
  a.coeffs[0] = (a.coeffs[0] + (1 ^ (a1&1))) & 3;

  for(i=0; i<NTESTS; i++)
  {
    t[i] = cpucycles();
    poly_Rq_inv(&r, &a);
  }
  print_results("poly_Rq_inv: ", t, NTESTS);

  for(i=0; i<NTESTS; i++)
  {
    t[i] = cpucycles();
    poly_S3_inv(&r, &a);
  }
  print_results("poly_S3_inv: ", t, NTESTS);

  for(i=0; i<NTESTS; i++)
  {
    t[i] = cpucycles();
    randombytes(fgbytes, NTRU_SAMPLE_FG_BYTES);
  }
  print_results("randombytes for fg: ", t, NTESTS);


  for(i=0; i<NTESTS; i++)
  {
    t[i] = cpucycles();
    randombytes(rmbytes, NTRU_SAMPLE_RM_BYTES);
  }
  print_results("randombytes for rm: ", t, NTESTS);

  for(i=0; i<NTESTS; i++)
  {
    t[i] = cpucycles();
    sample_iid(&a, fgbytes);
  }
  print_results("sample_iid: ", t, NTESTS);

#ifdef NTRU_HRSS
  for(i=0; i<NTESTS; i++)
  {
    t[i] = cpucycles();
    sample_iid_plus(&a, fgbytes);
  }
  print_results("sample_iid_plus: ", t, NTESTS);
#endif

#ifdef NTRU_HPS
  for(i=0; i<NTESTS; i++)
  {
    t[i] = cpucycles();
    sample_fixed_type(&a, fgbytes);
  }
  print_results("sample_fixed_type: ", t, NTESTS);
#endif

  for(i=0; i<NTESTS; i++)
  {
    t[i] = cpucycles();
    poly_lift(&a, &b);
  }
  print_results("poly_lift: ", t, NTESTS);

  for(i=0; i<NTESTS; i++)
  {
    t[i] = cpucycles();
    poly_Rq_to_S3(&a, &b);
  }
  print_results("poly_Rq_to_S3: ", t, NTESTS);

  for(i=0; i<NTESTS; i++)
  {
    t[i] = cpucycles();
    poly_Rq_sum_zero_tobytes(cts, &a);
  }
  print_results("poly_Rq_sum_zero_tobytes: ", t, NTESTS);

  for(i=0; i<NTESTS; i++)
  {
    t[i] = cpucycles();
    poly_Rq_sum_zero_frombytes(&a, cts);
  }
  print_results("poly_Rq_sum_zero_frombytes: ", t, NTESTS);

  for(i=0; i<NTESTS; i++)
  {
    t[i] = cpucycles();
    poly_S3_tobytes(cts, &b);
  }
  print_results("poly_S3_tobytes: ", t, NTESTS);

  for(i=0; i<NTESTS; i++)
  {
    t[i] = cpucycles();
    poly_S3_frombytes(&b, cts);
  }
  print_results("poly_S3_frombytes: ", t, NTESTS);*/

  /* Close file to save file data */
  fclose(fPtr);


  /* Success message */
  printf("File created and saved successfully. :) \n");

  free(pks);
  free(sks);
  free(cts);

  return 0;
}
