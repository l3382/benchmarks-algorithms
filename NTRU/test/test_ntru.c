#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>

#include "../params.h"
#include "../kem.h"

/* returns 0 for equal strings, 1 for non-equal strings */
static unsigned char verify(const unsigned char *a, const unsigned char *b, size_t len)
{
  uint64_t r;
  size_t i;

  r = 0;
  for(i=0;i<len;i++)
    r |= a[i] ^ b[i];

  r = (~r + 1); // Two's complement
  r >>= 63;
  return (unsigned char)r;
}

#define TRIALS 100
int main(void)
{
  /* File pointer to hold reference to our file */
    FILE * fPtr;

    /* 
     * Open file in w (write) mode. 
     * "data/file1.txt" is complete path to create file
     */
    fPtr = fopen("output.txt", "w");

    /* fopen() return NULL if last operation was unsuccessful */
    if(fPtr == NULL)
    {
        /* File not created hence exit */
        printf("Unable to create file.\n");
        exit(EXIT_FAILURE);
    }

  int i,c;
  unsigned char* pk = (unsigned char*) malloc(NTRU_PUBLICKEYBYTES);
  unsigned char* sk = (unsigned char*) malloc(NTRU_SECRETKEYBYTES);
  unsigned char* ct = (unsigned char*) malloc(NTRU_CIPHERTEXTBYTES);
  unsigned char* k1 = (unsigned char*) malloc(NTRU_SHAREDKEYBYTES);
  unsigned char* k2 = (unsigned char*) malloc(NTRU_SHAREDKEYBYTES);

  fprintf(fPtr, "SECRETKEYBYTES: %d \n", NTRU_SECRETKEYBYTES);
  fprintf(fPtr, "PUBLICKEYBYTES: %d \n", NTRU_PUBLICKEYBYTES);
  fprintf(fPtr, "CIPHERTEXTBYTES: %d \n", NTRU_CIPHERTEXTBYTES);


  crypto_kem_keypair(pk, sk);

  c = 0;
  clock_t t0, t1, t2, t3;
  for(i=0; i<TRIALS; i++)
  {
    t1 = clock();
    crypto_kem_enc(ct, k1, pk);
    t2 = clock()-t1 + t2;

    t0 = clock();
    crypto_kem_dec(k2, ct, sk);
    t3 = clock()-t0 + t3;

    c += verify(k1, k2, NTRU_SHAREDKEYBYTES);

  }
  fprintf(fPtr, "CHIFFREMENT:%ld \n", t1);
  fprintf(fPtr, "DECHIFFREMENT:%ld \n", t0);
  if (c > 0)
    printf("ERRORS: %d/%d\n\n", c, TRIALS);
  else
    printf("success\n\n");

  free(sk);
  free(pk);
  free(ct);
  free(k1);
  free(k2);

    /* Close file to save file data */
    fclose(fPtr);


    /* Success message */
    printf("File created and saved successfully. :) \n");

  return 0;
}
