#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

int main()
{
    system("rm TXTFiles/NTRUREF-hps2048509.txt");
    system("rm TXTFiles/NTRUREF-hps2048677.txt");
    system("rm TXTFiles/NTRUREF-hps4096821.txt");
    system("rm TXTFiles/NTRUREF-hrss701.txt");
    system("rm TXTFiles/NTRUREF-hrss1373.txt");
    system(" cd ref-hps2048509 && make && ./test/speed && mv output.txt ../TXTFiles/NTRUREF-hps2048509.txt");
    system(" cd ref-hps2048677 && make && ./test/speed && mv output.txt ../TXTFiles/NTRUREF-hps2048677.txt");
    system(" cd ref-hps4096821 && make && ./test/speed && mv output.txt ../TXTFiles/NTRUREF-hps4096821.txt");
    system(" cd ref-hrss701 && make && ./test/speed && mv output.txt ../TXTFiles/NTRUREF-hrss701.txt");
    system(" cd ref-hrss1373 && make && ./test/speed && mv output.txt ../TXTFiles/NTRUREF-hrss1373.txt");
    return 0;
}