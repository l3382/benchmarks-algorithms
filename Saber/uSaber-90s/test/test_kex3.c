#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#include "../api.h"
#include "../poly.h"
#include "../rng.h"
#include "../SABER_indcpa.h"
#include "../verify.h"
#include "cpucycles.c"

// void fprintBstr(char *S, unsigned char *A, unsigned long long L)
// {
// 	unsigned long long  i;

// 	printf("%s", S);

// 	for ( i=0; i<L; i++ )
// 		printf("%02X", A[i]);

// 	if ( L == 0 )
// 		printf("00");

// 	printf("\n");
// }

uint64_t clock1,clock2;
uint64_t clock_kp_mv,clock_cl_mv, clock_kp_sm, clock_cl_sm;

static int test_kem_cca()
{

  int CYPHER_TEXT; 
  uint8_t pk[SABER_PUBLICKEYBYTES];
  uint8_t sk[SABER_SECRETKEYBYTES];
  uint8_t c[SABER_BYTES_CCA_DEC];	
  uint8_t k_a[SABER_KEYBYTES], k_b[SABER_KEYBYTES];
	
  unsigned char entropy_input[48];
	
  uint64_t i, j,m,n, repeat;
  repeat=1000;	
  uint64_t CLOCK1,CLOCK2;
  uint64_t CLOCK_kp,CLOCK_enc,CLOCK_dec;
  uint64_t CLOCK_enc2[10000],CLOCK_dec2[10000];

  	CLOCK1 = 0;
        CLOCK2 = 0;
	CLOCK_kp = CLOCK_enc = CLOCK_dec = 0;
	clock_kp_mv=clock_cl_mv=0;
	clock_kp_sm = clock_cl_sm = 0;


   
	time_t t;
   	// Intializes random number generator
   	srand((unsigned) time(&t));

    	for (i=0; i<48; i++){
        	//entropy_input[i] = rand()%256;
        	entropy_input[i] = i;
	}
    	randombytes_init(entropy_input, NULL, 256);
	const char* filename = "SABERRef3.txt";
    	FILE* output_file = fopen(filename, "w");
    	if (!output_file) {
        	perror("fopen");
        	exit(EXIT_FAILURE);
    	}
    fprintf(output_file,"SECRETKEYBYTES:%d\n",SABER_SECRETKEYBYTES);
    fprintf(output_file,"PUBLICKEYBYTES:%d\n",SABER_PUBLICKEYBYTES);
  	for(i=0; i<repeat; i++)
  	{
	    //printf("i : %lu\n",i);

	    //Generation of secret key sk and public key pk pair
	    CLOCK1=cpucycles();	
	    crypto_kem_keypair(pk, sk);
	    CLOCK2=cpucycles();	
	    CLOCK_kp=CLOCK_kp+(CLOCK2-CLOCK1);	


	    //Key-Encapsulation call; input: pk; output: ciphertext c, shared-secret k_a;	
	    CLOCK1=cpucycles();
	    crypto_kem_enc(c, k_a, pk);
	    CLOCK2=cpucycles();	
	    CLOCK_enc=CLOCK_enc+(CLOCK2-CLOCK1);	
		CLOCK_enc2[i]=(CLOCK2-CLOCK1);
		CYPHER_TEXT = 0;
		for(j=0; j<SABER_BYTES_CCA_DEC; j++){
			CYPHER_TEXT += 1;
		}
		

	    //Key-Decapsulation call; input: sk, c; output: shared-secret k_b;	
	    CLOCK1=cpucycles();
	    crypto_kem_dec(k_b, c, sk);
	    CLOCK2=cpucycles();	
	    CLOCK_dec=CLOCK_dec+(CLOCK2-CLOCK1);	
	  	CLOCK_dec2[i]=(CLOCK2-CLOCK1);

		
	    // Functional verification: check if k_a == k_b?
	    for(j=0; j<SABER_KEYBYTES; j++)
	    {
		//printf("%u \t %u\n", k_a[j], k_b[j]);
		if(k_a[j] != k_b[j])
		{
			printf("----- ERR CCA KEM ------\n");
			return 0;	
			break;
		}
	    }
		//printf("\n");
  	}
	for (m=0 ; m < repeat; m++)
	  {
	    for (n=0 ; n < repeat-m-1; n++)
	    {
	      /* Pour un ordre décroissant utiliser < */
	      if (CLOCK_dec2[n] > CLOCK_dec2[n+1]) 
	      {
		tmp = CLOCK_dec2[n];
		CLOCK_dec2[n] = CLOCK_dec2[n+1];
		CLOCK_dec2[n+1] = tmp;
	      }
	    }
	  }
	  for (i=0 ; i < repeat; i++)
	  {
	    for (j=0 ; j < repeat-i-1; j++)
	    {
	      /* Pour un ordre décroissant utiliser < */
	      if (CLOCK_enc2[j] > CLOCK_enc2[j+1]) 
	      {
		tmp = CLOCK_enc2[j];
		CLOCK_enc2[j] = CLOCK_enc2[j+1];
		CLOCK_enc2[j+1] = tmp;
	      }
	    }
	  }
	fprintf(output_file,"KEYPAIR:%lu \n",CLOCK_kp/repeat);
	fprintf(output_file,"CHIFFREMENT:%lu :%lu \n",CLOCK_enc/repeat,CLOCK_enc2[5000]);
	fprintf(output_file,"DECHIFFREMENT:%lu :%lu \n",CLOCK_dec/repeat,CLOCK_dec2[5000]);
	fclose(output_file);
  	return 0;
}

/*
void test_kem_cpa(){

	uint8_t pk[SABER_PUBLICKEYBYTES];
	uint8_t sk[SABER_SECRETKEYBYTES];

	indcpa_kem_keypair(unsigned char *pk, unsigned char *sk);
	indcpa_kem_enc(unsigned char *message_received, unsigned char *noiseseed, const unsigned char *pk, unsigned char *ciphertext)
	indcpa_kem_dec(const unsigned char *sk, const unsigned char *ciphertext, unsigned char message_dec[])
}
*/
int main()
{

	test_kem_cca();
    /* Success message */
    printf("File created and saved successfully. :) \n");
	return 0;
}
