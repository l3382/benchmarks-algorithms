#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "../sign.h"
#include "../poly.h"
#include "../polyvec.h"
#include "../params.h"
#include "cpucycles.h"
#include "speed_print.h"

#define NTESTS 1000

uint64_t t[NTESTS];

int main(int argc, char ** argv)
{
  unsigned int i;
  size_t siglen;
  uint8_t pk[CRYPTO_PUBLICKEYBYTES];
  uint8_t sk[CRYPTO_SECRETKEYBYTES];
  uint8_t sig[CRYPTO_BYTES];
  int t1;
  
  /* File pointer to hold reference to our file */
  FILE * fPtr;

  /* 
    * Open file in w (write) mode. 
    * "data/file1.txt" is complete path to create file
    */

  if (argc != 2) {
    printf("Make sure there is only 1 argument.");
  }

  fPtr = fopen(argv[1], "w");

  /* fopen() return NULL if last operation was unsuccessful */
  if(fPtr == NULL)
  {
      /* File not created hence exit */
      printf("Unable to create file.\n");
      exit(EXIT_FAILURE);
  }

  fprintf(fPtr, "PUBLICKEYBYTES: %d \n", CRYPTO_PUBLICKEYBYTES);
  fprintf(fPtr, "SECRETKEYBYTES: %d \n", CRYPTO_SECRETKEYBYTES);
  fprintf(fPtr, "CIPHERTEXTBYTES: %d \n", CRYPTO_BYTES);

  for(i = 0; i < NTESTS; ++i) {
    t1 = clock();
    crypto_sign_keypair(pk, sk);  
    t[i] = clock() - t1;
  } 
  print_results("KEYGENERATIONTIME:", t, NTESTS, fPtr);

  for(i = 0; i < NTESTS; ++i) {
    t1 = clock();
    crypto_sign_signature(sig, &siglen, sig, CRHBYTES, sk);
    t[i] = clock() - t1;
  }
  print_results("SIGNATURETIME:", t, NTESTS, fPtr);

  for(i = 0; i < NTESTS; ++i) {
    t1 = clock();
    crypto_sign_verify(sig, CRYPTO_BYTES, sig, CRHBYTES, pk);
    t[i] = clock() - t1;
  }
  print_results("VERIFICATIONTIME:", t, NTESTS, fPtr);

  fprintf(fPtr, "PLAINTEXTBYTES: %d \n", CRHBYTES);

  /* Close file to save file data */
  fclose(fPtr);

  /* Success message */
  printf("File created and saved successfully. :) \n");

  return 0;
}
