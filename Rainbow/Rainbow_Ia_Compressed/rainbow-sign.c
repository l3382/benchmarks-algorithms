///  @file rainbow-sign.c
///  @brief A command-line tool for signing a file.
///

#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include "rainbow_config.h"
#include "utils.h"
#include "api.h"

#define NTESTS 100

void print_results(const char *s, uint64_t *t, size_t tlen, FILE* fPtr)
{

  if(tlen < 2) {
    fprintf(stderr, "ERROR: Need a least two cycle counts!\n");
    return;
  }
  size_t i;
  long int acc=0;

  fprintf(fPtr, "%s ", s);
  for(i=0;i<tlen-1;i++){
	acc += t[i];
	fprintf(fPtr, "%llu ", t[i]);
  }
  fprintf(fPtr, "\n%s %ld\n", s, acc/tlen);
}

int main( int argc , char ** argv )
{

	if( !(3 == argc) ) {
		printf("Usage:\n\n\trainbow-sign sk_file_name file_to_be_signed\n\n");
		return -1;
	}

	/* File pointer to hold reference to our file */
    FILE * fPtr;
	uint64_t t[NTESTS];
	/* 
     * Open file in w (write) mode. 
     * "data/file1.txt" is complete path to create file
     */
    fPtr = fopen("RAINBOWIa_Compressed.txt", "a");

    /* fopen() return NULL if last operation was unsuccessful */
    if(fPtr == NULL)
    {
        /* File not created hence exit */
        printf("Unable to create file 2.\n");
        exit(EXIT_FAILURE);
    }

	uint8_t *_sk = (uint8_t*)malloc( CRYPTO_SECRETKEYBYTES );

	FILE * fp;
	int r = 0;

	fp = fopen( argv[1] , "r");
	if( NULL == fp ) {
		printf("fail to open secret key file.\n");
		return -1;
	}
	r = byte_fget( fp ,  _sk , CRYPTO_SECRETKEYBYTES );
	fclose( fp );
	if( CRYPTO_SECRETKEYBYTES != r ) {
		printf("fail to load key file.\n");
		return -1;
	}

	unsigned char * msg = NULL;
	unsigned long long mlen = 0;
	r = byte_read_file( &msg , &mlen , argv[2] );
	if( 0 != r ) {
		printf("fail to read message file.\n");
		return -1;
	}

	unsigned char * signature = malloc( mlen + CRYPTO_BYTES );
	if( NULL == signature ) {
		printf("alloc memory for signature buffer fail.\n");
		return -1;
	}

	unsigned long long smlen = 0;
	int t1;
	for (int i = 0; i < NTESTS; i++)
	{
		t1 = clock();
		crypto_sign( signature, &smlen, msg , mlen , _sk );
		t[i] = clock() - t1;
	}
	print_results("SIGNATURETIME:", t, NTESTS, fPtr);
	
	if( 0 != r ) {
		printf("sign() fail.\n");
		return -1;
	}

	byte_fdump( stdout , CRYPTO_ALGNAME " signature"  , signature + mlen , CRYPTO_BYTES );
	printf("\n");

	free( msg );
	free( signature );
	free( _sk );

	return 0;
}

