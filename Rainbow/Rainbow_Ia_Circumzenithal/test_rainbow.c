#include <stdio.h>
#include <stdint.h>
#include <time.h>

#include "rainbow_config.h"

#include "utils.h"

#include "rng.h"

#include "api.h"

int main()
{
    system("./rainbow-genkey pk_file_name sk_file_name \n");
    system("./rainbow-sign sk_file_name message.txt | tee message_sign\n");
    system("./rainbow-verify pk_file_name message_sign message.txt\n");
    return 0;
}