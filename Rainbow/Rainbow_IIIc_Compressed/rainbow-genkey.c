///  @file rainbow-genkey.c
///  @brief A command-line tool for generating key pairs.
///

#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include "rainbow_config.h"
#include "utils.h"
#include "rng.h"
#include "api.h"

#define NTESTS 100

void print_results(const char *s, uint64_t *t, size_t tlen, FILE* fPtr)
{

  if(tlen < 2) {
    fprintf(stderr, "ERROR: Need a least two cycle counts!\n");
    return;
  }
  size_t i;
  long int acc=0;

  fprintf(fPtr, "%s ", s);
  for(i=0;i<tlen;i++){
	acc += t[i];
	fprintf(fPtr, "%llu ", t[i]);
  }
  fprintf(fPtr, "\n%s %ld\n", s, acc/tlen);
}



int main( int argc , char ** argv )
{

	if( !((3 == argc) || (4 == argc)) ) {
		printf("Usage:\n\n\trainbow-genkey pk_file_name sk_file_name [random_seed_file]\n\n");
		return -1;
	}

	/* File pointer to hold reference to our file */
	uint64_t t[NTESTS];
    FILE * fPtr;
	/* 
     * Open file in w (write) mode. 
     * "data/file1.txt" is complete path to create file
     */
    fPtr = fopen("RAINBOWIIIc_Compressed.txt", "w");

    /* fopen() return NULL if last operation was unsuccessful */
    if(fPtr == NULL)
    {
        /* File not created hence exit */
        printf("Unable to create file 1.\n");
        exit(EXIT_FAILURE);
    }

	fprintf(fPtr,"SECRETKEYBYTES: %lu\n", CRYPTO_SECRETKEYBYTES );
	fprintf(fPtr,"PUBLICKEYBYTES: %lu\n",  CRYPTO_PUBLICKEYBYTES );


	// set random seed
	unsigned char rnd_seed[48] = {0};
	int rr = byte_from_binfile( rnd_seed , 48 , (4==argc)? argv[3] : "/dev/random" );
	if( 0 != rr ) printf("read seed file fail.\n");
	randombytes_init( rnd_seed , NULL , 256 );


	uint8_t *_sk = (uint8_t*)malloc( CRYPTO_SECRETKEYBYTES );
	uint8_t *_pk = (uint8_t*)malloc( CRYPTO_PUBLICKEYBYTES );
	FILE * fp;
	int t1;

	for (int i = 0; i < NTESTS; i++)
	{
		t1 = clock();
		crypto_sign_keypair( _pk, _sk );
		t[i] = clock() - t1;
	}
	print_results("GENERATIONTIME:", t, NTESTS, fPtr);


	fp = fopen( argv[1] , "w+");
	if( NULL == fp ) {
		printf("fail to open public key file.\n");
		return -1;
	}
	byte_fdump( fp , CRYPTO_ALGNAME " public key" , _pk , CRYPTO_PUBLICKEYBYTES );
	fclose( fp );

	fp = fopen( argv[2] , "w+");
	if( NULL == fp ) {
		printf("fail to open secret key file.\n");
		return -1;
	}
	//ptr = (unsigned char *)&sk;
	//sprintf(msg,"%s secret key", name);
	byte_fdump( fp ,  CRYPTO_ALGNAME " secret key" , _sk , CRYPTO_SECRETKEYBYTES );
	fclose( fp );

	printf("generate %s pk/sk success.\n" , CRYPTO_ALGNAME );

	free( _sk );
	free( _pk );

	return 0;
}

