#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

int main()
{
    system("cd Rainbow_Ia_Circumzenithal && make && ./test_rainbow && mv RAINBOWIa_Circumzenithal.txt ../TXTFiles/RAINBOWIa_Circumzenithal.txt");
    system("cd Rainbow_Ia_Classic && make && ./test_rainbow && mv RAINBOWIa_Classic.txt ../TXTFiles/RAINBOWIa_Classic.txt");
    system("cd Rainbow_Ia_Compressed && make && ./test_rainbow && mv RAINBOWIa_Compressed.txt ../TXTFiles/RAINBOWIa_Compressed.txt");
    system("cd Rainbow_IIIc_Circumzenithal && make && ./test_rainbow && mv RAINBOWIIIc_Circumzenithal.txt ../TXTFiles/RAINBOWIIIc_Circumzenithal.txt");
    system("cd Rainbow_IIIc_Classic && make && ./test_rainbow && mv RAINBOWIIIc_Classic.txt ../TXTFiles/RAINBOWIIIc_Classic.txt");
    system("cd Rainbow_IIIc_Compressed && make && ./test_rainbow && mv RAINBOWIIIc_Compressed.txt ../TXTFiles/RAINBOWIIIc_Compressed.txt");
    system("cd Rainbow_Vc_Circumzenithal && make && ./test_rainbow && mv RAINBOWVc_Circumzenithal.txt ../TXTFiles/RAINBOWVc_Circumzenithal.txt");
    system("cd Rainbow_Vc_Classic && make && ./test_rainbow && mv RAINBOWVc_Classic.txt ../TXTFiles/RAINBOWVc_Classic.txt");
    system("cd Rainbow_Vc_Compressed && make && ./test_rainbow && mv RAINBOWVc_Compressed.txt ../TXTFiles/RAINBOWVc_Compressed.txt");
    return 0;
}