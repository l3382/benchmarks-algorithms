///  @file rainbow-verify.c
///  @brief A command-line tool for verifying a signature.
///

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <time.h>
#include "rainbow_config.h"
#include "utils.h"
#include "api.h"

#define NTESTS 100

void print_results(const char *s, uint64_t *t, size_t tlen, FILE* fPtr)
{

  if(tlen < 2) {
    fprintf(stderr, "ERROR: Need a least two cycle counts!\n");
    return;
  }
  size_t i;
  long int acc=0;

  fprintf(fPtr, "%s ", s);
  for(i=0;i<tlen-1;i++){
	acc += t[i];
	fprintf(fPtr, "%llu ", t[i]);
  }
  fprintf(fPtr, "\n%s %ld\n", s, acc/tlen);
}

int main( int argc , char ** argv )
{

	if( 4 != argc ) {
                printf("Usage:\n\n\trainbow-verify pk_file_name signature_file_name message_file_name\n\n");
                return -1;
        }

	/* File pointer to hold reference to our file */
    FILE * fPtr;
	uint64_t t[NTESTS];
	/* 
     * Open file in w (write) mode. 
     * "data/file1.txt" is complete path to create file
     */
    fPtr = fopen("RAINBOWVc_Compressed.txt", "a");

    /* fopen() return NULL if last operation was unsuccessful */
    if(fPtr == NULL)
    {
        /* File not created hence exit */
        printf("Unable to create file 2.\n");
        exit(EXIT_FAILURE);
    }

	uint8_t * pk = (uint8_t *) malloc( CRYPTO_PUBLICKEYBYTES );

	FILE * fp;
	int r;

	fp = fopen( argv[1] , "r");
	if( NULL == fp ) {
		printf("fail to open public key file.\n");
		return -1;
	}
	r = byte_fget( fp ,  pk , CRYPTO_PUBLICKEYBYTES );
	fclose( fp );
	if( CRYPTO_PUBLICKEYBYTES != r ) {
		printf("fail to load key file.\n");
		return -1;
	}

	unsigned char * msg = NULL;
	unsigned long long mlen = 0;
	r = byte_read_file( &msg , &mlen , argv[3] );
	if( 0 != r ) {
		printf("fail to read message file.\n");
		return -1;
	}

	unsigned char * signature = malloc( mlen + CRYPTO_BYTES );
	if( NULL == signature ) {
		printf("alloc memory for signature buffer fail.\n");
		return -1;
	}
	memcpy( signature , msg , mlen );
	fp = fopen( argv[2] , "r");
	if( NULL == fp ) {
		printf("fail to open signature file.\n");
		return -1;
	}
	r = byte_fget( fp ,  signature + mlen , CRYPTO_BYTES );
	fclose( fp );
	if( CRYPTO_BYTES != r ) {
		printf("fail to load signature file.\n");
		return -1;
	}

	int t1;
	for (int i = 0; i < NTESTS; i++)
	{
		t1 = clock();
		crypto_sign_open( msg , &mlen , signature , mlen + CRYPTO_BYTES , pk );
		t[i] = clock() - t1;
	}
	print_results("VERIFICATIONTIME:", t, NTESTS, fPtr);
	
	free( msg );
	free( signature );
	free( pk );

}

