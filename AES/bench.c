 /*********************************************************************
 * Copyright (c) 2016 Pieter Wuille                                   *
 * Distributed under the MIT software license, see the accompanying   *
 * file COPYING or https://opensource.org/licenses/mit-license.php.   *
 **********************************************************************/
#include <stdio.h>
#include <math.h>
#include "sys/time.h"
#include <time.h>
#include "ctaes.h"
#include <string.h>
#include <stdlib.h>
static double gettimedouble(void) {
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return tv.tv_usec * 0.000001 + tv.tv_sec;
}

static void print_number(double x,FILE* output_file5) {
    double y = x;
    int c = 0;
    if (y < 0.0) {
        y = -y;
    }
    while (y < 100.0) {
        y *= 10.0;
        c++;
    }
    fprintf(output_file5,"%.*f", c, x);
}
char *randstring(size_t length) {

    static char charset[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";        
    char *randomString = NULL;

    if (length) {
        randomString = malloc(sizeof(char) * (length +1));

        if (randomString) {            
            for (int n = 0;n < length;n++) {            
                int key = rand() % (int)(sizeof(charset) -1);
                randomString[n] = charset[key];
            }
        }
    }

    return randomString;
}

static void run_benchmark(char *name,char *name2,char *type,FILE* output_file4, void (*benchmark)(void*), void (*setup)(void*), void (*teardown)(void*), void* data, int count, int iter) {
    int i,j;
    double tmp;
    char txt[512];
    char pwd[512];
    if(name!="no"){
    	fprintf(output_file4,"SECRETKEYBYTES:%s\n",name);
    	fprintf(output_file4,"PUBLICKEYBYTES:0\n");
    	fprintf(output_file4,"CYPHERTEXTBYTES:%s\n",name);
    }
    double min = HUGE_VAL;
    double sum = 0.0;
    double sum2 = 0.0;
    double tab[count];
    double tab2[count];
    for (i = 0; i < count; i++) {
        double begin, total;
        double begin2, total2;
        if(name=="128"){
    		begin2 = gettimedouble();
        	randstring(16);
        	total2 = gettimedouble() - begin2;
    	}
    	if(name=="192"){
    		begin2 = gettimedouble();
        	randstring(24);
        	total2 = gettimedouble() - begin2;
    	}
    	if(name=="256"){
    		begin2 = gettimedouble();
        	randstring(32);
        	total2 = gettimedouble() - begin2;
    	}
    	if(name!="no"){
    		tab2[i]=total2;
    		sum2+=total2;
    	}
        if (setup != NULL) {
            setup(data);
        }
        begin = gettimedouble();
        benchmark(data);
        total = gettimedouble() - begin;
        if (teardown != NULL) {
            teardown(data);
        }
        tab[i]=total;
        sum += total;
    }
    if(name!="no"){
    fprintf(output_file4,"GENERATION:");
    for (int n = 0;n < count;n++) {            
        print_number(tab2[n]/1000*CLOCKS_PER_SEC,output_file4);
        fprintf(output_file4," ");
     }
     fprintf(output_file4,"\nGENERATION:");
    print_number((sum2 / count)/1000*CLOCKS_PER_SEC,output_file4);
     fprintf(output_file4," \n");
    }
    snprintf(pwd,sizeof(pwd),"%s"," \n");
    snprintf(txt,sizeof(txt),"%s",type);
    fprintf(output_file4,"%s",txt);
     for (int n = 0;n < count;n++) {            
        print_number(tab[n]/1000*CLOCKS_PER_SEC,output_file4);
        fprintf(output_file4," ");
     }
    /*if(count%2==0){
    	print_number((tab[count/2-1]+tab[count/2])/2/1000*CLOCKS_PER_SEC,output_file4);
    }
    else{
    	print_number((tab[count/2]/1000*CLOCKS_PER_SEC),output_file4);
    }
    snprintf(pwd,sizeof(pwd),"%s"," cycles/ticks:");*/
    snprintf(txt,sizeof(txt),"%s",type);
    fprintf(output_file4,"\n%s",txt);
    //fprintf(output_file4,"%s",pwd);
    print_number((sum / count)/1000*CLOCKS_PER_SEC,output_file4);
    snprintf(pwd,sizeof(pwd),"%s"," \n");
    fprintf(output_file4,"%s",pwd);
    if(name=="no"){
    	fprintf(output_file4,"SECURITY:%s\n",name2);
    }
}

static void bench_AES128_init(void* data) {
    AES128_ctx* ctx = (AES128_ctx*)data;
    int i;
    for (i = 0; i < 50000; i++) {
        AES128_init(ctx, (unsigned char*)ctx);
    }
}

static void bench_AES128_encrypt_setup(void* data) {
    AES128_ctx* ctx = (AES128_ctx*)data;
    static const unsigned char key[16] = {0};
    AES128_init(ctx, key);
}

static void bench_AES128_encrypt(void* data) {
    const AES128_ctx* ctx = (const AES128_ctx*)data;
    unsigned char scratch[16] = {0};
    int i;
    for (i = 0; i < 4000000 / 16; i++) {
        AES128_encrypt(ctx, 1, scratch, scratch);
    }
}

static void bench_AES128_decrypt(void* data) {
    const AES128_ctx* ctx = (const AES128_ctx*)data;
    unsigned char scratch[16] = {0};
    int i;
    for (i = 0; i < 4000000 / 16; i++) {
        AES128_decrypt(ctx, 1, scratch, scratch);
    }
}

static void bench_AES192_init(void* data) {
    AES192_ctx* ctx = (AES192_ctx*)data;
    int i;
    for (i = 0; i < 50000; i++) {
        AES192_init(ctx, (unsigned char*)ctx);
    }
}

static void bench_AES192_encrypt_setup(void* data) {
    AES192_ctx* ctx = (AES192_ctx*)data;
    static const unsigned char key[24] = {0};
    AES192_init(ctx, key);
}

static void bench_AES192_encrypt(void* data) {
    const AES192_ctx* ctx = (const AES192_ctx*)data;
    unsigned char scratch[16] = {0};
    int i;
    for (i = 0; i < 4000000 / 16; i++) {
        AES192_encrypt(ctx, 1, scratch, scratch);
    }
}

static void bench_AES192_decrypt(void* data) {
    const AES192_ctx* ctx = (const AES192_ctx*)data;
    unsigned char scratch[16] = {0};
    int i;
    for (i = 0; i < 4000000 / 16; i++) {
        AES192_decrypt(ctx, 1, scratch, scratch);
    }
}

static void bench_AES256_init(void* data) {
    AES256_ctx* ctx = (AES256_ctx*)data;
    int i;
    for (i = 0; i < 50000; i++) {
        AES256_init(ctx, (unsigned char*)ctx);
    }
}


static void bench_AES256_encrypt_setup(void* data) {
    AES256_ctx* ctx = (AES256_ctx*)data;
    static const unsigned char key[32] = {0};
    AES256_init(ctx, key);
}

static void bench_AES256_encrypt(void* data) {
    const AES256_ctx* ctx = (const AES256_ctx*)data;
    unsigned char scratch[16] = {0};
    int i;
    for (i = 0; i < 4000000 / 16; i++) {
        AES256_encrypt(ctx, 1, scratch, scratch);
    }
}

static void bench_AES256_decrypt(void* data) {
    const AES256_ctx* ctx = (const AES256_ctx*)data;
    unsigned char scratch[16] = {0};
    int i;
    for (i = 0; i < 4000000 / 16; i++) {
        AES256_decrypt(ctx, 1, scratch, scratch);
    }
}

int main(void) {
    AES128_ctx ctx128;
    AES192_ctx ctx192;
    AES256_ctx ctx256;
    const char* filename = "AESTXT/aes128.txt";
    FILE* output_file = fopen(filename, "w");
    if (!output_file) {
        perror("fopen");
        exit(EXIT_FAILURE);
    }
    run_benchmark("128","128","CHIFFREMENT:",output_file, bench_AES128_encrypt, bench_AES128_encrypt_setup, NULL, &ctx128, 20, 4000000);
    run_benchmark("no","128","DECHIFFREMENT:",output_file, bench_AES128_decrypt, bench_AES128_encrypt_setup, NULL, &ctx128, 20, 4000000);
    fclose(output_file);
    const char* filename2 = "AESTXT/aes192.txt";
    FILE* output_file2 = fopen(filename2, "w");
    if (!output_file) {
        perror("fopen");
        exit(EXIT_FAILURE);
    }
    run_benchmark("192","192","CHIFFREMENT:",output_file2, bench_AES192_encrypt, bench_AES192_encrypt_setup, NULL, &ctx192, 20, 4000000);
    run_benchmark("no","192","DECHIFFREMENT:",output_file2, bench_AES192_decrypt, bench_AES192_encrypt_setup, NULL, &ctx192, 20, 4000000);
    fclose(output_file2);
    const char* filename3 = "AESTXT/aes256.txt";
    FILE* output_file3 = fopen(filename3, "w");
    if (!output_file) {
        perror("fopen");
        exit(EXIT_FAILURE);
    }
    run_benchmark("256","256","CHIFFREMENT:",output_file3, bench_AES256_encrypt, bench_AES256_encrypt_setup, NULL, &ctx256, 20, 4000000);
    run_benchmark("no","256","DECHIFFREMENT:",output_file3, bench_AES256_decrypt, bench_AES256_encrypt_setup, NULL, &ctx256, 20, 4000000);
    fclose(output_file3);
    return 0;
}
